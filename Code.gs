var cc = DataStudioApp.createCommunityConnector()
var scriptProperties = PropertiesService.getScriptProperties()
scriptProperties.setProperty('douano', 'https://<my-douano>.douano.com')
scriptProperties.setProperty('client_id', '1')
scriptProperties.setProperty('secret', '<my_secret>')

function doGet (e) {
  if (e.parameter['code']) {
    return authCallback(e)
  } else {
    return HtmlService.createHtmlOutput('NO authorization code received')
  }
}

function setCredentials (request) {

}

OAuth2.getRedirectUri = function (optScriptId) {
  //REPLACE WITH THE URL of your WEB APP script
  return 'https://script.google.com/macros/s/xxxxxxxxxx/dev';
}

function isAdminUser () {
  return true
}

/**
 * Returns true if the auth service has access.
 * @return {boolean} True if the auth service has access.
 */
function isAuthValid () {
  return getOAuthService().hasAccess()
}

/**
 * Returns the Auth Type of this connector.
 * @return {object} The Auth type.
 */
function getAuthType () {
  return cc.newAuthTypeResponse().setAuthType(cc.AuthType.OAUTH2).build()
}

/**
 * Resets the auth service.
 */
function resetAuth () {
  getOAuthService().reset()
}

/**
 *
 * @return {OAuth2Service}
 */
function getOAuthService () {

  var douanoServer = scriptProperties.getProperty('douano')
  var clientId = scriptProperties.getProperty('client_id')
  var clientSecret = scriptProperties.getProperty('secret')
  var service = OAuth2.createService('My Douano connection').
    setAuthorizationBaseUrl(`${douanoServer}/authorize`).
    setTokenUrl(`${douanoServer}/oauth/token`).
    setClientId(clientId).
    setClientSecret(clientSecret).
    setPropertyStore(PropertiesService.getScriptProperties()).
    setCallbackFunction('authCallback')
  return service
}

/**
 * The OAuth callback.
 * @param {object} request The request data received from the OAuth flow.
 * @return {HtmlOutput} The HTML output to show to the user.
 */
function authCallback (request) {
  var authorized = getOAuthService().handleCallback(request)
  if (authorized) {
    return HtmlService.createHtmlOutput('Success! You can close this tab.')
  } else {
    return HtmlService.createHtmlOutput('Denied. You can close this tab')
  }

}

/**
 * Gets the 3P authorization URL.
 * @return {string} The authorization URL.
 * @see https://developers.google.com/apps-script/reference/script/authorization-info
 */
function get3PAuthorizationUrls () {
  return getOAuthService().getAuthorizationUrl()
}

function getConfig () {
  var config = cc.getConfig()
  config.setDateRangeRequired(true)
  return config.build()
}

function getFields () {
  var fields = cc.getFields()
  var types = cc.FieldType
  var aggregations = cc.AggregationType

  fields.newDimension().setId('invoice_number').setName('Factuurnummer').setType(types.TEXT)

  fields.newDimension().setId('invoice_date').setName('Factuurdatum').setType(types.YEAR_MONTH_DAY)

  fields.newDimension().setId('tag').setName('Klantnummer').setType(types.TEXT)

  fields.newDimension().setId('company').setName('Bedrijf').setType(types.TEXT)

  fields.newDimension().setId('country').setName('Land').setType(types.TEXT)

  fields.newDimension().setId('city').setName('Stad').setType(types.TEXT)

  fields.newDimension().setId('price_class').setName('Prijsklasse').setType(types.TEXT)

  fields.newDimension().setId('sku').setName('Artikelnummer').setType(types.TEXT)

  fields.newDimension().setId('description').setName('Omschrijving').setType(types.TEXT)

  fields.newDimension().setId('quantity').setName('Hoeveelheid').setType(types.NUMBER)

  fields.newDimension().setId('price').setName('Prijs').setType(types.NUMBER)

  fields.newDimension().setId('excise').setName('Accijnsen').setType(types.NUMBER)

  fields.newDimension().setId('discount').setName('Korting').setType(types.NUMBER)

  fields.newDimension().setId('discount_type').setName('Type korting').setType(types.TEXT)

  fields.newDimension().setId('financial_discount').setName('Financiële korting').setType(types.NUMBER)

  fields.newDimension().setId('commercial_discount').setName('Commerciële korting').setType(types.NUMBER)

  fields.newDimension().setId('total').setName('Omzet').setType(types.NUMBER)

  return fields
}

/**
 * Returns the schema for the given request.
 *
 * @param {Object} request Schema request parameters.
 * @returns {Object} Schema for the given request.
 */

function getSchema (request) {
  var fields = getFields().build()
  return { schema: fields }
}

function getData (request) {
  try {
    var douanoServer = scriptProperties.getProperty('douano')
    var token = getOAuthService().getAccessToken()
    var url = `${douanoServer}/api/public/v1/exporter/revenue?start_date=${request.dateRange.startDate}&end_date=${request.dateRange.endDate}`
    var options = {
      'method': 'get',
      'contentType': 'application/json',
      // Convert the JavaScript object to a JSON string.
      'headers': {
        'Authorization': `Bearer ${token}`
      }
    }
    var apiResponse = UrlFetchApp.fetch(url, options)
    var jsonResponse = JSON.parse(apiResponse)

    var fieldIds = request.fields.map(field => {return field.name})

    var requestedFields = getFields().forIds(fieldIds)

    var rows = jsonResponse.map(dataPoint => {
      return {
        values: fieldIds.map(fieldId => {
          if (fieldId === 'invoice_date' || fieldId === 'order_date' || fieldId === 'date') {
            var datePieces = dataPoint[fieldId].split('-')
            return `${datePieces[2]}${datePieces[1]}${datePieces[0]}`
          } else {
            return dataPoint[fieldId]
          }
        })
      }
    })

  } catch (e) {
    cc.newUserError().setDebugText('Error fetching data from API. Exception details: ' + e).setText(
      'The connector has encountered an unrecoverable error. Please try again later, or file an issue if this error persists.'
    ).throwException()
  }

  return {
    schema: requestedFields.build(),
    rows: rows
  }
}
